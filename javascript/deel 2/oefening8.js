let getal1;
do {
    getal1 = parseInt(prompt('Geef een eerste geheel getal in.'))
} while (typeof getal1 !== 'number' || isNaN(getal1))
let getal2;
do {
    getal2 = parseInt(prompt('Geef een tweede geheel getal in.'))
} while (typeof getal2 !== 'number' || isNaN(getal2))
let result1 = getal1 + getal2;
let result2 = getal1 - getal2;
let result3 = getal1 * getal2;
let result4 = getal1 / getal2;
document.getElementById('zin1').innerHTML = 'De uitkomst van de optelling is: ' + result1.toString();
document.getElementById('zin2').innerHTML = 'De uitkomst van de aftrekking is: ' + result2.toString();
document.getElementById('zin3').innerHTML = 'De uitkomst van de vermenigvuldiging is: ' + result3.toString();
document.getElementById('zin4').innerHTML = 'De uitkomst van de deling is: ' + result4.toString();