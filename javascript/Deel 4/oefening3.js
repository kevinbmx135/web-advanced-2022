//Deel 1
document.getElementById('red').style.color = "darkred";
//Deel 2
let bron = document.getElementById('weg');
bron.remove();
//Deel 3
let hoofding = document.createElement('h1');
let hoofdingtekst = document.createTextNode('Welkom pandiX')
hoofding.appendChild(hoofdingtekst);
document.getElementById('header').appendChild(hoofding);
//Deel 4
let footer = document.createElement('aside');
let footertekst = document.createTextNode('Adresgegevens PXL-Digital: Elfde-liniestraat 35 3500 HASSELT')
footer.appendChild(footertekst);
document.getElementById('main').appendChild(footer);

