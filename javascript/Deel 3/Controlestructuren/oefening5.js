let getal;
do {
    getal = parseInt(prompt('Gelieve een random geheel getal in te geven'));
    if (getal < 50) {
        alert('Te laag.');
    } else if (getal === 50) {
        alert('Proficiat, goed gegokt!');
    } else if (getal > 50) {
        alert('Te hoog.');
    } else {
        alert('Geen correct getal ingegeven!')
    }
} while (typeof getal !== 'number' || isNaN(getal) || getal !== 50)