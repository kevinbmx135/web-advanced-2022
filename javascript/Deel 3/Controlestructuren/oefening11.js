let aantalLijnen;
let hashtags = '';
do {
    aantalLijnen = parseInt(prompt('Gelieve een geheel getal tussen 1-10 in te geven'));
} while (typeof aantalLijnen !== 'number' || isNaN(aantalLijnen))
if (aantalLijnen < 1 || aantalLijnen > 10) {
    console.log('Niet mogelijk');
} else {
    for (let i = 0; i < aantalLijnen; i++) {
        console.log(hashtags += '#');
    }
}