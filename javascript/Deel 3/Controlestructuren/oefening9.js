let getal;
do {
    getal = parseInt(prompt('Gelieve een geheel getal tussen 1-10 in te geven'));
} while (typeof getal !== 'number' || isNaN(getal))
if (getal < 1 || getal > 10) {
    console.log('Niet mogelijk');
} else {
    document.getElementById('zin').innerHTML = getal + '<br>' + getal * 2 + '<br>' + getal * 3 + '<br>' + getal * 4 + '<br>' + getal * 5 + '<br>' + getal * 6 + '<br>' + getal * 7 + '<br>' + getal * 8 + '<br>' + getal * 9 + '<br>' + getal * 10;
}
