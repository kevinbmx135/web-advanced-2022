const d = new Date();
let uren = d.getHours();
let dag = d.getDay();
let groeting;
if (uren === 7 || 8 || 9 || 10 || 11) {
    groeting = 'Goedemorgen,';
} else if (uren === 12 || 13 || 14 || 15 || 16 || 17) {
    groeting = 'Goede dag,';
} else if (uren === 18 || 19 || 20 || 21 || 22 || 23 || 0 || 1 || 2 || 3 || 4 || 5 || 6) {
    groeting = 'Goede avond,';
}
switch (dag) {
    case 0:
        dag = 'zondag';
        break;
    case 1:
        dag = 'maandag';
        break;
    case 2:
        dag = 'dinsdag';
        break;
    case 3:
        dag = 'woensdag';
        break;
    case 4:
        dag = 'donderdag';
        break;
    case 5:
        dag = 'vrijdag';
        break;
    case 6:
        dag = 'zaterdag';
        break;
}
document.getElementById('zin').innerHTML = groeting + ' de dag van vandaag is  ' + dag + '.';
