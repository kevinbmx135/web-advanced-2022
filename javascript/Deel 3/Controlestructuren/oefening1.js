let geboortejaar;
do {
    geboortejaar = parseFloat(prompt('Gelieve uw geboortejaar in te geven.'));
} while (typeof geboortejaar !== 'number' || isNaN(geboortejaar))
let leeftijd = 2022 - geboortejaar;
if (leeftijd > 20) {
    alert('Je bent oud genoeg om de informatie op deze webpagina te bekijken.');
} else {
    alert('U bent dus ' + leeftijd + ' jaar.');
}
