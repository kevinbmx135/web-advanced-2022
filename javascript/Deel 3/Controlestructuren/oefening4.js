const d = new Date();
const uren = d.getHours();
const minuten = d.getMinutes();
const seconden = d.getSeconds();
let deeldag;
let groeting;
if (uren < 12) {
    deeldag = 'voormiddag';
} else if (uren >= 12) {
    deeldag = 'namiddag';
}
if (uren === 7 || 8 || 9 || 10 || 11) {
    groeting = 'Goedemorgen,';
} else if (uren === 12 || 13 || 14 || 15 || 16 || 17) {
    groeting = 'Goede dag,';
} else if (uren === 18 || 19 || 20 || 21 || 22 || 23 || 0 || 1 || 2 || 3 || 4 || 5 || 6) {
    groeting = 'Goede avond,';
}
document.getElementById('zin').innerHTML = groeting + ' het is tijdstip is ' + uren + ' uur ' + minuten + ' minuten ' + seconden + ' seconden en het is ' + deeldag + '.';
