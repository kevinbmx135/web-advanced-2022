let rassen = new Array(10)
rassen[0] = 'Hondenras 1';
rassen[1] = 'Hondenras 2';
rassen[2] = 'Hondenras 3';
rassen[3] = 'Hondenras 4';
rassen[4] = 'Hondenras 5';
rassen[5] = 'Hondenras 6';
rassen[6] = 'Hondenras 7';
rassen[7] = 'Hondenras 8';
rassen[8] = 'Hondenras 9';
rassen[9] = 'Hondenras 10';
let getal;
do {
    getal = parseInt(prompt('Gelieve een geheel getal tussen 1-10 in te geven'));
} while (typeof getal !== 'number' || isNaN(getal))
let ras = rassen[getal - 1];
if (getal < 1 || getal > 10) {
    console.log('Sorry, katten niet toegelaten!');
} else {
    document.getElementById('zin').innerHTML = 'U hebt gekozen voor: ' + ras;
}
