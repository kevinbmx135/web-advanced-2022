let rassen1 = new Array(10)
rassen1[0] = 'Hondenras 1';
rassen1[1] = 'Hondenras 2';
rassen1[2] = 'Hondenras 3';
rassen1[3] = 'Hondenras 4';
rassen1[4] = 'Hondenras 5';
rassen1[5] = 'Hondenras 6';
rassen1[6] = 'Hondenras 7';
rassen1[7] = 'Hondenras 8';
rassen1[8] = 'Hondenras 9';
rassen1[9] = 'Hondenras 10';
//2 elementen toevoegen op positie 5
rassen1.splice(5, 0, 'Extra hondenras 1', 'Extra hondenras 2');

let rassen2 = new Array(10)
rassen2[0] = 'Hondenras 1';
rassen2[1] = 'Hondenras 2';
rassen2[2] = 'Hondenras 3';
rassen2[3] = 'Hondenras 4';
rassen2[4] = 'Hondenras 5';
rassen2[5] = 'Hondenras 6';
rassen2[6] = 'Hondenras 7';
rassen2[7] = 'Hondenras 8';
rassen2[8] = 'Hondenras 9';
rassen2[9] = 'Hondenras 10';
//2 elementen verwijderen op positie 5
rassen2.splice(5, 2);
console.log(rassen1);
console.log(rassen2);