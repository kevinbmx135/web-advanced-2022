let getal;
do {
    getal = parseInt(prompt('Gelieve een random geheel getal in te geven'));
} while (typeof getal !== 'number' || isNaN(getal))

function verschil() {
    return 100 - getal;
}

console.log(verschil());